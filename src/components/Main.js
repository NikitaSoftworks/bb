import React from 'react';
import Character from "./Character";
import Episodes from "./Episodes";
import Pagination from "./Pagination";
import EmptyResults from "./EmptyResults";

const EPISODES_PER_PAGE = 6;

export default function Main ({character, episodes, page, onPageChange}) {
  if (!character || !episodes) {
    return <EmptyResults/>;
  }

  return (
    <div className="main">
      <Character character={character}/>

      <div className="episodes">
        <Episodes episodes={episodes.slice((page - 1) * EPISODES_PER_PAGE, page * EPISODES_PER_PAGE)}/>
        <Pagination
          totalPages={Math.ceil(episodes.length / EPISODES_PER_PAGE)}
          onPageChange={onPageChange}
          page={page}
        />
      </div>
    </div>
  );
}