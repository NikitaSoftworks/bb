import React from 'react';

export default function EmptyResults () {
  return (
    <div className="empty-results">No results</div>
  );
}