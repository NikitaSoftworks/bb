import React from 'react';

export default function Episodes ({ episodes }) {
  return (
    <div className="episodes-list">
      {episodes.map((e, i) => {
        return (
          <div className="episode" key={i}>
            <div>{e.title}</div>
            <div>{e.season} season</div>
            <div>{e.episode} episode</div>
            <div>Release {e.air_date}</div>
          </div>
        );
      })}
    </div>
  );
}