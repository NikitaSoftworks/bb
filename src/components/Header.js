import React from 'react';

export default function Header ({ value, onChange, onEnter }) {
  /**
   *
   * @param {Event} e
   */
  const onKeyDown = e => {
    if (e.key === 'Enter') {
      onEnter();
    }
  }

  return (
    <div className="header">
      <div className="logo"/>
      <input value={value} onChange={e => onChange(e.target.value)} onKeyDown={onKeyDown}/>
    </div>
  );
}