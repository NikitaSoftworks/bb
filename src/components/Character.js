import React from 'react';

export default function Character ({ character }) {
  return (
    <div className="character">
      <img src={character.img} alt=""/>
      <div>{character.name} ({character.nickname})</div>
      <div>Bd: {character.birthday}</div>
    </div>
  );
}