import React, {useCallback} from 'react';

export default function Pagination ({ page, totalPages, onPageChange }) {

  /**
   *
   * @param {number} number
   * @returns {JSX.Element}
   */
  const makeLink = useCallback(number => {
    return (
      // eslint-disable-next-line jsx-a11y/anchor-is-valid
      <a
        className={`page-link ${number === page ? 'active' : ''}`}
        onClick={() => onPageChange(number)}
        key={number}
        href="#"
      >
        {number}
      </a>
    );
  }, [page, onPageChange]);

  const arrowLeft = <button className="arrow left" disabled={page === 1} onClick={() => onPageChange(page - 1)}/>;

  const arrowRight = (
    <button className="arrow left" disabled={page === totalPages} onClick={() => onPageChange(page + 1)}/>
  );

  let firstNumber = page - 2;
  firstNumber = firstNumber < 1 ? 1 : firstNumber;
  let lastNumber = page + 2;
  lastNumber = lastNumber > totalPages ? totalPages : lastNumber;

  const links = [];
  for (let i = firstNumber; i <= lastNumber; i++) {
    links.push(makeLink(i));
  }

  return (
    <div className="pagination">
      {arrowLeft}
      {links}
      {arrowRight}
    </div>
  );
}