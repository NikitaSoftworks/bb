import React, { useEffect, useState, useCallback } from 'react';
import './App.css';
import Header from "./components/Header";
import Loader from "./components/Loader";
import Main from "./components/Main";

const API_ROOT = 'https://www.breakingbadapi.com/api/';
const ROUTES = {
  CHARACTERS: 'characters',
  EPISODES: 'episodes',
};

function App () {
  const [name, setName] = useState('');
  const [awaitingResponses, setAwaitingResponses] = useState(0);
  const [character, setCharacter] = useState(null);
  const [episodes, setEpisodes] = useState(null);
  const [page, setPage] = useState(1);

  /**
   *
   * @param {string} url
   * @param {Object} params
   * @returns {Promise<void>}
   */
  const makeApiRequest = useCallback((url, params) => {
    setAwaitingResponses(a => a + 1);

    const query = new URLSearchParams(params);
    return fetch(`${url}?${query}`)
      .then(response => response.json())
      .finally(() => setAwaitingResponses(a => a - 1));
  }, []);

  /**
   *
   */
  const loadCharacter = useCallback(() => {
    if (!name) {
      return;
    }

    makeApiRequest(`${API_ROOT}${ROUTES.CHARACTERS}`, {name}).then(data => {
      if (!data.length) {
        setCharacter(null);
        return;
      }

      setCharacter(data[0]);
    });
  }, [name]);

  /**
   *
   */
  const loadEpisodes = useCallback(() => {
    if (!name) {
      return;
    }

    makeApiRequest(`${API_ROOT}${ROUTES.EPISODES}`, {name, page}).then(setEpisodes);
  }, [name, page]);

  /**
   *
   */
  const loadAllData = useCallback(() => {
    loadCharacter();
    loadEpisodes();
  }, []);

  useEffect(() => loadEpisodes(), [page]);

  let content;
  if (!awaitingResponses) {
    content = <Main character={character} episodes={episodes} page={page} onPageChange={setPage}/>;
  } else {
    content = <Loader/>;
  }

  return (
    <div className="app">
      <Header value={name} onEnter={loadAllData} onChange={setName}/>
      {content}
    </div>
  );
}

export default App;
